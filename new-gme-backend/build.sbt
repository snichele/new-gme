


lazy val commonSettings = Seq(
  scalaVersion := "2.11.7",
  sbtVersion := "0.13.9",
  organization := "pacifica",
  name := "new-gme-backend",
  version := "1.0"
)

lazy val root = (project in file(".")).
  configs(IntegrationTest).
  settings(commonSettings: _*).
  settings(Defaults.itSettings: _*).
  settings(
    libraryDependencies ++= Seq(
      "org.springframework.boot" % "spring-boot-starter-web" % "1.3.2.RELEASE",
      "org.springframework.boot" % "spring-boot-starter-test" % "1.3.2.RELEASE",
      "org.springframework.boot" % "spring-boot-starter-security" % "1.3.2.RELEASE",
      "org.webjars" % "bootstrap" % "3.1.1",
      "org.thymeleaf" % "thymeleaf-spring4" % "2.1.2.RELEASE",
      "nz.net.ultraq.thymeleaf" % "thymeleaf-layout-dialect" % "1.2.1",
      "org.springframework.boot" % "spring-boot-starter-tomcat" % "1.3.2.RELEASE" % "provided",
      "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided",
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.7.2",
      "org.apache.tomcat.embed" % "tomcat-embed-core" % "8.0.30" % "container",
      "org.apache.tomcat.embed" % "tomcat-embed-logging-juli" % "8.0.30" % "container",
      "org.apache.tomcat.embed" % "tomcat-embed-jasper" % "8.0.30" % "container",
      "org.scalatest" %% "scalatest" % "2.2.6" % "it,test",
      "com.jayway.jsonpath" % "json-path" % "2.2.0" % "it,test"
    )
  ).enablePlugins(TomcatPlugin)
