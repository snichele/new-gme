package newgme.model


import java.util.Date

import scala.beans.BeanProperty

case class FormMetaDatas(@BeanProperty name: String, @BeanProperty version: Version = Version()) {}

case class Version(major: Int = 0, minor: Int = 0, releaseDate: Option[Date] = Option.empty)