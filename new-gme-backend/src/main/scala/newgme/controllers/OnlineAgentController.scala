package newgme.controllers

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}

@Controller
@RequestMapping(Array("/online-agent"))
class OnlineAgentController /* @Autowired()*/ () {

  @RequestMapping(method = Array(RequestMethod.GET))
  def home(model: Model) = {
    "online-agent/index"
  }

}