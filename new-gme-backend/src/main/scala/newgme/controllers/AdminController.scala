package newgme.controllers

import newgme.model.FormMetaDatas
import newgme.service.FormsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, ResponseBody}

@Controller
@RequestMapping(Array("/admin"))
class AdminController @Autowired()(formsService: FormsService) {

  @RequestMapping(method = Array(RequestMethod.GET))
  def home(model: Model) = {
    "admin/index"
  }

  @RequestMapping(method = Array(RequestMethod.GET), value = Array("/hello"))
  @ResponseBody
  def hello() = {
    "Hello !"
  }

  @RequestMapping(method = Array(RequestMethod.GET), value = Array("/forms/list"))
  @ResponseBody
  def getAllFormsMetaDatas(): Set[FormMetaDatas] = {
    if (isFormAdminRole())
      formsService.getAllFormsMetasDatas()
    else
      Set.empty
  }

  /** TODO Extract role from security role. */
  private def isFormAdminRole(): Boolean = true

  // should better check SecurityContextHolder.getContext.getAuthentication.getPrincipal.asInstanceOf[UserDetails].getAuthorities

}