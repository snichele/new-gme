package newgme.controllers

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}

@Controller
@RequestMapping(Array("/central"))
class CentralController /* @Autowired()*/ () {

  @RequestMapping(method = Array(RequestMethod.GET))
  def home(model: Model) = {
    "central/home"
  }

}