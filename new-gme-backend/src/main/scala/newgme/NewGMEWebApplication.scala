package newgme

import org.springframework.boot.SpringApplication

case class NewGMEWebApplication() {
}

object NewGMEWebApplication extends App {
  SpringApplication.run(classOf[NewGMEConfig])
}