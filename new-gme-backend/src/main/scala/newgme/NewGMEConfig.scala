package newgme

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.{Bean, ComponentScan, Configuration}
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

@Configuration
@EnableAutoConfiguration
@ComponentScan
class NewGMEConfig {

  @Bean
  def jsonConverter(): MappingJackson2HttpMessageConverter = {
    val jsonConverter = new MappingJackson2HttpMessageConverter()
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    jsonConverter.setObjectMapper(objectMapper)
    jsonConverter
  }

}