package newgme.service

import newgme.model.FormMetaDatas
import org.springframework.stereotype.Service

trait FormsService {
  def getAllFormsMetasDatas(): Set[FormMetaDatas]
}

@Service
class StubFormsService extends FormsService {
  override def getAllFormsMetasDatas() = Set(
    FormMetaDatas("Dummy form")
  )
}
