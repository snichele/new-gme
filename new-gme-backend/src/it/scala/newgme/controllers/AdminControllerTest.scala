package newgme.controllers

import newgme.NewGMEConfig
import org.scalatest.{BeforeAndAfter, FunSpec}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.web.{AnnotationConfigWebContextLoader, WebAppConfiguration}
import org.springframework.test.context.{ContextConfiguration, TestContextManager}
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers._
import org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import org.springframework.web.context.WebApplicationContext
import org.hamcrest.Matchers.is
import org.hamcrest.Matchers.nullValue

@ContextConfiguration(
  classes = Array(classOf[NewGMEConfig]),
  loader = classOf[AnnotationConfigWebContextLoader]
)
@WebAppConfiguration
class AdminControllerTest extends FunSpec with BeforeAndAfter {

  var mockMvc: MockMvc = null

  @Autowired val webApplicationContext: WebApplicationContext = null

  before {
    this.mockMvc = webAppContextSetup(webApplicationContext).build()
  }

  describe("NewGMEWebApplication webApplicationContext") {

    it("could be instantiated") {
      assert(webApplicationContext != null)
    }

    describe("exposes REST service for the admin type user") {

      it("that can say 'hello' ! ") {
        mockMvc.perform(get("/admin/hello"))
          .andExpect(status().isOk)
          .andExpect(content().bytes("Hello !".map(_.toByte).toArray))
      }

      it("that return the list of all declared forms accessible by the form admin role.") {
        println(mockMvc.perform(get("/admin/forms/list"))
          .andExpect(status().isOk)
          .andExpect(jsonPath("$[0].name", is("Dummy form")))
          .andExpect(jsonPath("$[0].version.major", is(0)))
          .andExpect(jsonPath("$[0].version.minor", is(0)))
          .andExpect(jsonPath("$[0].version.releaseDate", nullValue()))
          .andReturn().getResponse.getContentAsString)
      }

    }
  }

  new TestContextManager(this.getClass).prepareTestInstance(this)

}