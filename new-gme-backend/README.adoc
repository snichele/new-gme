= New GME Backend

The backend of the New GME application


== What is it about ?

The server exposes the online version of the forms for the agents.
It can communicate with the electron (or whatever other it is) client via rest services.

=== Actors

Admin : the administrator of the forms definitions.

Online-agent : an agent who is using the application directly through the server (not using the electron client).


== Using sbt as the build tool:

Start up the sbt console using `sbt`
Run the Spring boot main program using `run`
Run it-tests (to test controllers, for example) with `it:test`




